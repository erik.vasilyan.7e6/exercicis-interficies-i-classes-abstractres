/*
    * NOM: Erik Vasilyan
    * DATA: 2023-03-01
    * TITOL: Instrument Simulator
 */

abstract class Instrument {
    open fun makeSounds(times: Int) {}
}

class Triangle(private val resonance: Int): Instrument() {

    override fun makeSounds(times: Int) {
        repeat(times) {
            when (resonance) {
                1 -> println("TINC")
                2 -> println("TIINC")
                3 -> println("TIIINC")
                4 -> println("TIIIINC")
                5 -> println("TIIIIINC")
            }
        }
    }
}

class Drump(private val tone: String): Instrument() {

    override fun makeSounds(times: Int) {
        repeat(times) {
            when (tone) {
                "A" -> println("TAAAM")
                "O" -> println("TOOOM")
                "U" -> println("TUUUM")
            }
        }
    }

}

fun main() {
    val instruments: List<Instrument> = listOf(
        Triangle(5),
        Drump("A"),
        Drump("O"),
        Triangle(1),
        Triangle(5)
    )
    play(instruments)
}

private fun play(instruments: List<Instrument>) {
    for (instrument in instruments) {
        instrument.makeSounds(2) // plays 2 times the sound
    }
}