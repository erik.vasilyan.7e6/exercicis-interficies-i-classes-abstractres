/*
    * NOM: Erik Vasilyan
    * DATA: 2023-03-01
    * TITOL: Student With Text Grade
 */

data class Student (val name: String, val textGrade: GRADE)

enum class GRADE {
    FAILED, PASSED, GOOD, NOTABLE, EXCELLENT
}

fun main() {
    val firstStudent = Student("Mar", GRADE.FAILED)
    val secondStudent = Student("Joan", GRADE.EXCELLENT)

    println(firstStudent)
    println(secondStudent)
}