import java.util.Scanner

/*
    * NOM: Erik Vasilyan
    * DATA: 2023-03-02
    * TITOL: Quiz
 */

const val defaultColor = "\u001B[0m"
const val red = "\u001B[31m"
const val green = "\u001B[32m"
const val blue = "\u001B[34m"

abstract class Question(val enunciat: String, val correctAnswer: String) {
    abstract fun show()
    abstract fun askUserToAnswer()
    abstract fun isAnswerCorrect(userAnswer: String): Boolean
}

class FreeTextQuestion(enunciat: String, correctAnswer: String): Question(enunciat, correctAnswer) {
    override fun show() {
        println(enunciat)
    }

    override fun askUserToAnswer() {
        print("\nAnswer: ")
    }

    override fun isAnswerCorrect(userAnswer: String): Boolean {
        return userAnswer.uppercase() == super.correctAnswer.uppercase()
    }
}

class MultipleChoiseQuestion(enunciat: String, correctAnswer: String, wrongAnswer1: String, wrongAnswer2: String, wrongAnswer3: String): Question(enunciat, correctAnswer) {

    private val answers = mutableListOf(correctAnswer, wrongAnswer1, wrongAnswer2, wrongAnswer3).shuffled()

    override fun show() {
        println(enunciat)
        println("\n   1. ${answers[0]}")
        println("   2. ${answers[1]}")
        println("   3. ${answers[2]}")
        println("   4. ${answers[3]}")
    }

    override fun askUserToAnswer() {
        print("\nOption: ")
    }

    override fun isAnswerCorrect(userAnswer: String): Boolean {
        var answer = ""
        when (userAnswer) {
            "1" -> answer = answers[0]
            "2" -> answer = answers[1]
            "3" -> answer = answers[2]
            "4" -> answer = answers[3]
        }
        return answer.uppercase() == super.correctAnswer.uppercase()
    }
}

class Quiz {
    private var correctAnswers = 0

    val questions = mutableListOf(
        FreeTextQuestion("Whose period was known as the Golden Age of Rome?", "Augustus"),
        FreeTextQuestion("What were warriors in Japan known as?", "Samarais"),
        FreeTextQuestion("How old was Queen Elizabeth II?", "96"),
        FreeTextQuestion("Which Greek goddess was the Parthenon dedicated to?", "Athena"),
        FreeTextQuestion("What year did Google launch?", "1998"),
        MultipleChoiseQuestion("What year did the Chernobyl disaster occur?", "1986", "1983", "1991", "1974"),
        MultipleChoiseQuestion("The United States bought Alaska from which country?", "Russia", "France", "Armenia", "Italy"),
        MultipleChoiseQuestion("When is the first World war started?","1910","1918","1932","1948"),
        MultipleChoiseQuestion("Which Greek historian is known as the “Father of History” ?","Herodotus","Xenophon","Theopompus","Posidonius"),
        MultipleChoiseQuestion("What was the name of the first computer?","ENIAC","COMPUTX","NVIDIA","RADEON"),
    ).shuffled()

    fun sumCorrectAnswer() {
        correctAnswers += 1
    }

    fun showCorrectAnswers() {
        println("\n${blue}Respostes correctes: $correctAnswers$defaultColor")
    }
}

fun main() {
    val scanner = Scanner(System.`in`)

    val quiz = Quiz()
    val questions = quiz.questions

    println("Quiz Game")
    repeat(9) { print("-") }
    println("")

    var index = 1
    for (question in questions) {
        print("\n$index. ")
        question.show()

        question.askUserToAnswer()
        val userAnswer = scanner.next()
        val isAnswerCorrect = question.isAnswerCorrect(userAnswer)

        if (isAnswerCorrect) {
            println("\n${green}Correcte!$defaultColor")
            quiz.sumCorrectAnswer()
        }
        else println("\n${red}Incorrecte!$defaultColor")
        index++
    }
    quiz.showCorrectAnswers()
}