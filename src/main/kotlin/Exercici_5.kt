/*
    * NOM: Erik Vasilyan
    * DATA: 2023-03-02
    * TITOL: Autonomous Car Prototype
 */

interface CarSensors {

    fun isThereSomethingAt(direction: Direction) : Boolean
    fun go(direction : Direction)
    fun stop()
}

class AutonomousCar: CarSensors {

    fun doNextNSteps(n :Int) {
        for (i in 1 .. n) {
            if (!isThereSomethingAt(Direction.FRONT)) go(Direction.FRONT)
            else {
                if (!isThereSomethingAt(Direction.RIGHT)) go(Direction.RIGHT)
                else {
                    if (!isThereSomethingAt(Direction.LEFT)) go(Direction.LEFT)
                    else stop()
                }
            }
        }

    }

    override fun isThereSomethingAt(direction: Direction): Boolean {
        TODO("Check if is there something at passed direction")
    }

    override fun go(direction: Direction) {
        TODO("Go to passed direction")
    }

    override fun stop() {
        TODO("Stop the car")
    }
}

enum class Direction {
    FRONT, LEFT, RIGHT
}

fun main() {
    val car = AutonomousCar()

    car.doNextNSteps(60)
}