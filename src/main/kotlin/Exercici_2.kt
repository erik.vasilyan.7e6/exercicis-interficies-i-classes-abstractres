import java.util.*

/*
    * NOM: Erik Vasilyan
    * DATA: 2023-03-01
    * TITOL: Rainbow
 */

enum class RAINBOW(val color: String) {
    RED("red"),
    ORANGE("orange"),
    YELLOW("yellow"),
    GREEN("green"),
    BLUE("blue"),
    INDIGO("indigo"),
    VIOLET("violet")
}

fun main() {
    val scanner = Scanner(System.`in`)
    val userColor = scanner.next()

    val isColorInRainbow = RAINBOW.values().any { it.color == userColor }
    println(isColorInRainbow)
}