import java.util.*

/*
    * NOM: Erik Vasilyan
    * DATA: 2023-03-03
    * TITOL: Gym Control App
 */

interface GymControlReader {
    fun nextId() : String
}

class GymControlManualReader(private val scanner: Scanner = Scanner(System.`in`)) : GymControlReader {
    override fun nextId(): String = scanner.next()
}

fun main() {

    val gymControlManualReader = GymControlManualReader()
    val gymMembersIds = mutableSetOf<String>()

    for (i in 1 .. 8) {
        val gymMemberId = gymControlManualReader.nextId()

        if (gymMembersIds.contains(gymMemberId)) {
            gymMembersIds.remove(gymMemberId)
            println("$gymMemberId Sortida")
        }
        else {
            gymMembersIds.add(gymMemberId)
            println("$gymMemberId Entrada")
        }
    }
}